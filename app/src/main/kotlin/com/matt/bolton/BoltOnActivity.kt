
package com.matt.bolton

import java.util.UUID

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothManager
import android.bluetooth.le.ScanFilter
import android.bluetooth.le.ScanResult
import android.companion.AssociationRequest
import android.companion.BluetoothLeDeviceFilter
import android.companion.CompanionDeviceManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.IntentSender
import android.os.Build
import android.os.Bundle
import android.os.ParcelUuid
import android.view.MenuItem
import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.activity.result.IntentSenderRequest
import androidx.activity.result.contract.ActivityResultContract
import androidx.activity.result.contract.ActivityResultContracts.StartIntentSenderForResult
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.bottomnavigation.BottomNavigationView

// keeping because finding this import is an epic battle
// import androidx.activity.viewModels

import com.matt.bolton.bolt.FileListingType
import com.matt.bolton.bolt.BOLT_SERVICE_UUID

class BoltOnActivity : AppCompatActivity() {

    private val REQUEST_ENABLE_BT = 1
    private val REQUEST_SELECT_DEVICE = 2
    private val NOTIFICATION_CHANNEL_ID = "BoltOnNotifications"

    private val NUM_FILE_PAGES = 3
    private val pagesPosType = mapOf(
        0 to FileListingType.USB,
        1 to FileListingType.INTERNAL_ROUTES,
        2 to FileListingType.INTERNAL_PLANS
    )
    private val pagesPosMenuID = mapOf(
        0 to R.id.navigation_usb,
        1 to R.id.navigation_internal_routes,
        2 to R.id.navigation_internal_plans
    )
    private val pagesMenuIDPos
        = pagesPosMenuID.entries.associate{(k,v)-> v to k}

    private lateinit var fileListingPagerAdapter : FileListingPagerAdapter
    private lateinit var fileListingPager : ViewPager2
    private lateinit var fileActionInfo : View
    private lateinit var fileActionStatus : TextView
    private lateinit var fileActionProgress : ProgressBar
    private var scanning = false

    private lateinit var viewModel : BoltViewModel

    // for closing from notifications
    private val INTENT_CLOSE_ACTIVITY = "android.intent.CLOSE_ACTIVITY"
    private val closeReceiver = object : BroadcastReceiver() {
        override fun onReceive(context : Context, intent : Intent) {
            finish();
        }
    }

    // for clearing notifications when device goes
    private val disconnectReceiver = object : BroadcastReceiver() {
        override fun onReceive(context : Context, intent : Intent) {
            val device : BluetoothDevice?
                = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE)
            device?.let {
                if (it.getAddress().equals(viewModel.device?.getAddress()))
                    clearAllNotifications()
            }
        }
    }

    // for clearing notifications when bluetooth is turned off
    private val bluetoothOffReceiver = object : BroadcastReceiver() {
        override fun onReceive(context : Context, intent : Intent) {
            when (intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, -1)) {
                BluetoothAdapter.STATE_OFF -> clearAllNotifications()
            }
        }
    }

    private val isViewModelDisconnected : Boolean
        get() = (viewModel.connectionStatus.value == null ||
                 viewModel.connectionStatus.value ==
                    BoltViewModel.ConnectionStatus.DISCONNECTED)

    private val deviceManager : CompanionDeviceManager
        by lazy(LazyThreadSafetyMode.NONE) {
        getSystemService(CompanionDeviceManager::class.java)
    }

    val enableBTLauncher = registerForActivityResult(
        object : ActivityResultContract<Unit, Boolean>() {
            override fun createIntent(context : Context, u : Unit)
                = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)

            override fun parseResult(resultCode : Int, i : Intent?) : Boolean
                = resultCode == AppCompatActivity.RESULT_OK
        }
    ) { result : Boolean ->
        if (result) findDevice()
        else setClickConnect()
    }

    val selectDeviceLauncher = registerForActivityResult(
        object : ActivityResultContract<IntentSender, ScanResult?>() {
            val startContract = StartIntentSenderForResult()

            override fun createIntent(
                context : Context, chooserLauncher : IntentSender
            ) = startContract.createIntent(
                context,
                IntentSenderRequest.Builder(chooserLauncher).build()
            )

            override fun parseResult(
                resultCode : Int, data : Intent?
            ) : ScanResult? {
                return when (resultCode) {
                    AppCompatActivity.RESULT_OK -> data?.getParcelableExtra(
                        CompanionDeviceManager.EXTRA_DEVICE
                    )
                    else -> null
                }
            }
        }
    ) { scan : ScanResult? ->
        onSelectedDevice(scan)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        registerReceivers()
        createNotificationChannel()

        setContentView(R.layout.bolt_activity)
        initialiseUI()
        if (isViewModelDisconnected)
            startFindDevice()
    }

    override fun onPause() {
        super.onPause()
        notifyIfConnectionOpen()
    }

    override fun onResume() {
        super.onResume()
        clearAllNotifications()
    }

    override fun onDestroy() {
        super.onDestroy()
        clearAllNotifications()
    }

    override fun onBackPressed() {
        val pos = fileListingPager.getCurrentItem()
        val frag = fileListingPagerAdapter.getFragment(pos)
        val handled = frag?.onBackPressed()
        if (handled != true) {
            viewModel.disconnect()
            disassociateAllDevices()
            finish()
        }
    }

    /**
     * Reset UI to click/connect
     */
    private fun setClickConnect() {
        scanning = false
        getSupportActionBar()?.title = getResources().getString(R.string.click_connect)
    }

    private fun startFindDevice() {
        scanning = true
        getSupportActionBar()?.title = getResources().getString(R.string.scanning)

        val bluetoothManager
            = getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        val bluetoothAdapter = bluetoothManager.adapter

        if (bluetoothAdapter == null || !bluetoothAdapter.isEnabled) {
            enableBTLauncher.launch(Unit)
        } else {
            findDevice()
        }
    }

    private fun findDevice() {
        val scanFilter : ScanFilter
            = ScanFilter.Builder()
                        .setServiceUuid(ParcelUuid(BOLT_SERVICE_UUID))
                        .build()

        val deviceFilter : BluetoothLeDeviceFilter
            = BluetoothLeDeviceFilter.Builder()
                                     .setScanFilter(scanFilter)
                                     .build()

        val pairingRequest : AssociationRequest
            = AssociationRequest.Builder()
                                .addDeviceFilter(deviceFilter)
                                .build()

        deviceManager.associate(
            pairingRequest,
            object : CompanionDeviceManager.Callback() {

                override fun onDeviceFound(chooserLauncher : IntentSender) {
                    selectDeviceLauncher.launch(chooserLauncher)
                }

                override fun onFailure(error: CharSequence?) {
                    setClickConnect()
                }
            },
            null
        )
    }

    private fun onSelectedDevice(scan : ScanResult?) {
        scanning = false
        scan?.device?.let {
            viewModel.connect(it)
        } ?: run {
            setClickConnect()
        }
    }

    private fun onDeviceConnected() {
        getSupportActionBar()?.title = viewModel.device?.getName()
    }

    private fun onDeviceConnecting() {
        getSupportActionBar()?.title = getResources().getString(R.string.connecting)
    }

    private fun onDeviceDisconnected() {
        clearAllNotifications()
        if (!scanning) {
            setClickConnect()
        }
        disassociateAllDevices()
    }

    private fun disassociateAllDevices() {
        for (oldMac in deviceManager.getAssociations())
            deviceManager.disassociate(oldMac)
    }

    private fun initialiseUI() {
        val toolbar : Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        toolbar.setOnClickListener { _ ->
            if (isViewModelDisconnected)
                startFindDevice()
        }

        fileListingPager = findViewById(R.id.file_listing_pager) as ViewPager2
        val navigation : BottomNavigationView
            = findViewById(R.id.file_list_navigation)

        // TODO: create maps from page positions to ids and vice versa?
        fileListingPagerAdapter = FileListingPagerAdapter(this)
        fileListingPager.adapter = fileListingPagerAdapter
        fileListingPager.registerOnPageChangeCallback(
            object : ViewPager2.OnPageChangeCallback() {
                override fun onPageSelected(position : Int) {
                    navigation.setSelectedItemId(pagesPosMenuID[position]!!)
                }
            }
        )

        fileActionInfo = findViewById(R.id.file_action_info)
        fileActionStatus = findViewById(R.id.file_action_status)
        fileActionProgress = findViewById(R.id.file_action_progress)

        navigation.setOnNavigationItemSelectedListener(
            object : BottomNavigationView.OnNavigationItemSelectedListener {

            override fun onNavigationItemSelected(item : MenuItem) : Boolean {
                val pos = pagesMenuIDPos[item.getItemId()]!!
                fileListingPager.setCurrentItem(pos)
                return true
            }
        })

        // Set up UI
        viewModel = ViewModelProvider(this).get(BoltViewModel::class.java)

        viewModel.connectionStatus
                 .observe(this,
                          Observer<BoltViewModel.ConnectionStatus> { status ->
            when (status) {
                BoltViewModel.ConnectionStatus.DISCONNECTED -> {
                    onDeviceDisconnected()
                }
                BoltViewModel.ConnectionStatus.CONNECTING -> {
                    onDeviceConnecting()
                }
                BoltViewModel.ConnectionStatus.CONNECTED -> {
                    onDeviceConnected()
                }
                else -> { /* ignore */ }
            }
        })

        viewModel.fileActionInfo.observe(
            this,
            Observer<BoltViewModel.FileActionInfo>() { info ->
                if (info.status == BoltViewModel.FileActionStatus.FAILED) {
                    Toast.makeText(
                        this,
                        getResources().getString(R.string.file_action_failed),
                        Toast.LENGTH_SHORT
                    ).show()
                }
                updateFileActionInfo(info)
            }
        )
    }

    private inner class FileListingPagerAdapter(fa : FragmentActivity)
            : FragmentStateAdapter(fa) {
        val fragments : MutableMap<Int, FileListingFragment> = mutableMapOf()

        override fun getItemCount() : Int = NUM_FILE_PAGES

        override fun createFragment(position : Int) : Fragment {
            val fragment = FileListingFragment()
            val bundle = Bundle()
            val listType =  when (position) {
                0 -> FileListingType.USB
                1 -> FileListingType.INTERNAL_ROUTES
                else -> FileListingType.INTERNAL_PLANS
            }
            bundle.putSerializable(FILE_LISTING_FRAGMENT_LIST_TYPE, listType)
            fragment.arguments = bundle
            fragments[position] = fragment
            return fragment
        }

        fun getFragment(position : Int) : FileListingFragment? {
            return fragments[position]
        }
    }

    private fun updateFileActionInfo(info : BoltViewModel.FileActionInfo) {
        when (info.status) {
            BoltViewModel.FileActionStatus.SENDING -> {
                fileActionInfo.setVisibility(View.VISIBLE)
                fileActionStatus.text = getFileStatusText(R.string.file_sending,
                                                          info.filename)
                fileActionProgress.progress = info.progress
            }
            BoltViewModel.FileActionStatus.GETTING -> {
                fileActionInfo.setVisibility(View.VISIBLE)
                fileActionStatus.text = getFileStatusText(R.string.file_getting,
                                                          info.filename)
                fileActionProgress.progress = info.progress
            }
            BoltViewModel.FileActionStatus.FAILED -> {
                // TODO: Toast warning
                fileActionInfo.setVisibility(View.GONE)
            }
            else -> {
                fileActionInfo.setVisibility(View.GONE)
            }
        }
    }

    private fun getFileStatusText(prefixId : Int, filename : String?) : String {
        val cleanFilename = if (filename == null) "" else filename
        return getResources().getString(prefixId) +
               " " +
               cleanFilename
    }

    private fun createNotificationChannel() {
        // currently the app requires version O, but i'm keeping this
        // here in case things change.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = getString(R.string.channel_name)
            val descriptionText = getString(R.string.channel_description)
            val channel = NotificationChannel(
                NOTIFICATION_CHANNEL_ID,
                name,
                NotificationManager.IMPORTANCE_LOW
            ).apply {
                description = descriptionText
            }
            val notificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE)
                    as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }

    private fun notifyIfConnectionOpen() {
        if (isViewModelDisconnected)
            return

        // on click, restart existing activity
        val launchIntent = Intent(this, BoltOnActivity::class.java).apply {
            setAction(Intent.ACTION_MAIN);
            addCategory(Intent.CATEGORY_LAUNCHER);
        }
        val launchPendingIntent: PendingIntent
            = PendingIntent.getActivity(this, 0, launchIntent, 0)

        // exit activity via button
        val closeIntent = Intent(INTENT_CLOSE_ACTIVITY)
        val closePendingIntent
            = PendingIntent.getBroadcast(this, 0 , closeIntent, 0);

        var builder = NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
            .setSmallIcon(R.drawable.notification_icon)
            .setContentTitle(getString(R.string.connection_open_title))
            .setContentText(getString(R.string.connection_open_message))
            .setStyle(NotificationCompat.BigTextStyle()
                .bigText(getString(R.string.connection_open_message)))
            .setPriority(NotificationCompat.PRIORITY_LOW)
            .setContentIntent(launchPendingIntent)
            .setAutoCancel(true)
            .addAction(R.drawable.exit_icon,
                       getString(R.string.exit),
                       closePendingIntent)

        with(NotificationManagerCompat.from(this)) {
            // TODO: notification ids properly
            // notificationId is a unique int for each notification that you must define
            notify(1, builder.build())
        }
    }

    private fun clearAllNotifications() {
        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE)
                as NotificationManager
        notificationManager.cancelAll()
    }

    private fun registerReceivers() {
        val closeFilter = IntentFilter(INTENT_CLOSE_ACTIVITY)
        registerReceiver(closeReceiver, closeFilter)

        val disconnectFilter
            = IntentFilter(BluetoothDevice.ACTION_ACL_DISCONNECTED)
        registerReceiver(disconnectReceiver, disconnectFilter)

        val bluetoothOffFilter
            = IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED)
        registerReceiver(bluetoothOffReceiver, bluetoothOffFilter)
    }
}

