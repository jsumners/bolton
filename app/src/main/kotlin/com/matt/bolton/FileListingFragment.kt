
package com.matt.bolton

import java.text.DateFormat
import java.util.Date

import android.app.AlertDialog
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.activity.result.contract.ActivityResultContracts.CreateDocument
import androidx.activity.result.contract.ActivityResultContracts.GetContent
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine
import kotlinx.coroutines.launch

import com.matt.bolton.bolt.FileListingType

val FILE_LISTING_FRAGMENT_LIST_TYPE = "listType"

public class FileListingFragment() : Fragment() {

    private val REQUEST_FILE_TO_SEND = 101
    private val REQUEST_FILE_TO_GET = 102

    private val DATE_FORMAT = "HH:mm, d MMM yyyy"

    private lateinit var viewModel : BoltViewModel
    private lateinit var listType : FileListingType

    private lateinit var swipeContainer : SwipeRefreshLayout
    private lateinit var fileListingAdapter : FileListingAdapter
    private lateinit var fileListingView : RecyclerView
    private lateinit var sendButton : View
    private lateinit var backButton : ImageView
    private lateinit var pathView : TextView
    // don't allow dir changes while waiting
    private var waitingDirChange = false

    private val isViewModelConnected
        get() = (viewModel.connectionStatus.value ==
                 BoltViewModel.ConnectionStatus.CONNECTED)


    val pickFileToSendLauncher
        = registerForActivityResult(GetContent()) { uri : Uri? ->
            onFileToSend(uri)
        }
    val pickFileToGetLauncher
        = registerForActivityResult(CreateDocument()) { uri : Uri? ->
            onFileToGet(uri)
        }

    override fun onCreateView(inflater : LayoutInflater,
                              container : ViewGroup?,
                              savedInstanceState : Bundle?) : View {
        val view = inflater.inflate(R.layout.fragment_file_listing,
                                    container,
                                    false)

        val listTypeArg
            = getArguments()?.getSerializable(
                FILE_LISTING_FRAGMENT_LIST_TYPE) as FileListingType?
        if (listTypeArg == null)
            return view
        listType = listTypeArg

        val provider = ViewModelProvider(requireActivity())
        viewModel = provider.get(BoltViewModel::class.java)

        backButton = view.findViewById(R.id.up_level)
        pathView = view.findViewById(R.id.dirname)
        fileListingView = view.findViewById(R.id.file_listing)
        sendButton = view.findViewById(R.id.send_file)

        backButton.setOnClickListener { _ -> popDirectory() }
        sendButton.setOnClickListener { _ -> pickFileToSend() }

        val context = view.getContext()
        val layoutManager = LinearLayoutManager(context)
        fileListingView.setLayoutManager(layoutManager)
        fileListingView.setItemAnimator(DefaultItemAnimator())
        fileListingView.addItemDecoration(
            DividerItemDecoration(context,
                                  DividerItemDecoration.VERTICAL)
        )

        swipeContainer = view.findViewById(R.id.swipeContainer)
        swipeContainer.setOnRefreshListener(
             object : SwipeRefreshLayout.OnRefreshListener {
                 override fun onRefresh() {
                     viewModel.refreshFileListing(listType)
                 }
             }
         )

        fileListingAdapter= FileListingAdapter()
        fileListingView.adapter = fileListingAdapter

        viewModel.getFileListingLiveData(listType).observe(
            viewLifecycleOwner,
            Observer<BoltViewModel.BoltListing> { listing ->
                swipeContainer.setRefreshing(false);
                onUpdatedListing(listing)
            }
        )
        viewModel.connectionStatus
                 .observe(viewLifecycleOwner,
                          Observer<BoltViewModel.ConnectionStatus> { _ ->
            setConnectionUI()
        })
        setConnectionUI()
        viewModel.fileActionInfo.observe(
            viewLifecycleOwner,
            Observer<BoltViewModel.FileActionInfo> { _ ->
                showHideSendButton(!viewModel.isFileActionActive)
            }
        )

        return view
    }

    /**
     * Goes up a directory, returns false if at top
     */
    fun onBackPressed() : Boolean {
        return viewModel.popDirectory(listType)
    }

    private fun onUpdatedListing(listing : BoltViewModel.BoltListing) {
        waitingDirChange = false
        if (!listing.hasParent) {
            backButton.setImageResource(R.drawable.home_icon)
            backButton.contentDescription = "" // none needed
            pathView.text = getResources().getString(R.string.top_level)
        } else {
            backButton.setImageResource(R.drawable.back_icon)
            backButton.contentDescription = getResources().getString(R.string.directory_back_description)
            pathView.text = listing.path
        }
        fileListingAdapter.fileListing = listing.files
    }

    /**
     * Adapt UI based on whether connected
     */
    private fun setConnectionUI() {
        if (isViewModelConnected) {
            waitingDirChange = true
            viewModel.refreshFileListing(listType)
            showHideSendButton(true)
        } else {
            showHideSendButton(false)
        }
    }

    private fun pickFileToSend() {
        pickFileToSendLauncher.launch("*/*")
    }

    private fun onFileToSend(uri : Uri?) {
        uri?.let { viewModel.sendFile(it, listType) }
    }

    /**
     * Start choosing where to save a file
     *
     * Save fileEntry to the chosen path
     */
    private fun pickFileToGet(fileEntry : BoltViewModel.BoltFileEntry) {
        lifecycleScope.launch {
            val convert = askFITToGPX(fileEntry.filename)
            val suggestedFilename
                = if (convert)
                    viewModel.changeFITExtensionToGPX(fileEntry.filename)
                else
                    fileEntry.filename
            saveInfile(fileEntry)
            pickFileToGetLauncher.launch(suggestedFilename)
        }
    }

    /**
     * Perform the get action when file selected
     *
     * Relies on inFilename having been stored viewModel
     * before calling the file pick activity
     */
    private fun onFileToGet(uri : Uri?) {
        getSavedInFile()?.let { inFile ->
            uri?.let { uri ->
                viewModel.getFile(inFile, uri)
            }
        }
    }

    /**
     * Show or hide send button
     *
     * Will always hide if no connection
     */
    private fun showHideSendButton(show : Boolean) {
        if (show && isViewModelConnected)
            sendButton.setVisibility(View.VISIBLE)
        else
            sendButton.setVisibility(View.GONE)
    }

    private fun pushDirectory(dirname : String) {
        if (waitingDirChange)
            return
        waitingDirChange = true
        viewModel.pushDirectory(listType, dirname)
    }

    private fun popDirectory() {
        if (waitingDirChange)
            return
        waitingDirChange = false
        viewModel.popDirectory(listType)
    }

    /**
     * Before starting pick get file activity, call this
     *
     * Needs to store file so it's still there
     * if our activity gets killed.
     */
    private fun saveInfile(fileEntry : BoltViewModel.BoltFileEntry) {
        viewModel.inFileForGetting = fileEntry
    }

    /**
     * Read inFile to get from view model
     *
     * Should have been stored there before starting pick get file
     * activity. Null if none set.
     */
    private fun getSavedInFile() : BoltViewModel.BoltFileEntry? {
        return viewModel.inFileForGetting
    }

    private fun clearSavedInFilename() {
        viewModel.inFileForGetting = null
    }

    private fun deleteFile(fileEntry : BoltViewModel.BoltFileEntry) {
        lifecycleScope.launch {
            if (checkDeleteFile(fileEntry))
                viewModel.deleteFile(listType, fileEntry)
        }
    }

    private fun playFile(fileEntry : BoltViewModel.BoltFileEntry) {
        lifecycleScope.launch {
            viewModel.playFile(fileEntry)
        }
    }

    private suspend fun
    checkDeleteFile(fileEntry : BoltViewModel.BoltFileEntry) : Boolean {
        return suspendCoroutine { cont ->
            val msg = (getResources().getString(R.string.confirm_delete) +
                       " " +
                       fileEntry.filename)

            AlertDialog.Builder(requireActivity())
                       .setMessage(msg)
                       .setIcon(android.R.drawable.ic_dialog_alert)
                       .setPositiveButton(android.R.string.ok,
                                          { _, _ -> cont.resume(true) })
                       .setNegativeButton(android.R.string.cancel,
                                          { _, _ -> cont.resume(false) })
                       .show()
        }
    }

    private inner class FileListingAdapter
            : RecyclerView.Adapter<FileEntryHolder>() {

        var fileListing : List<BoltViewModel.BoltFileEntry> = listOf()
            set(value) {
                field = value.sortedWith(
                    compareBy({ !it.isDirectory }, { it.filename })
                )
                selectedPosition = -1
                notifyDataSetChanged()
            }

        var selectedPosition : Int = -1
            set(value) {
                if (field > -1)
                    notifyItemChanged(field)
                field = value
                notifyItemChanged(field)
            }

        override fun onCreateViewHolder(parent : ViewGroup,
                                        viewType : Int) : FileEntryHolder {
            val fileEntryView : View
                = LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.file_entry,
                                         parent,
                                         false)
            return FileEntryHolder(fileEntryView, this)
        }

        override fun onBindViewHolder(holder : FileEntryHolder,
                                      position : Int) {
            holder.fileEntry = fileListing.get(position)
        }

        override fun getItemCount() : Int {
            return fileListing.size
        }

        fun toggleSelectedPosition(position : Int) {
            if (selectedPosition == position)
                selectedPosition = -1
            else
                selectedPosition = position
        }
    }

    private inner class FileEntryHolder(view : View,
                                        val adapter : FileListingAdapter)
            : RecyclerView.ViewHolder(view) {

        private var icon : ImageView = view.findViewById(R.id.entry_icon)
        private var filenameView : TextView = view.findViewById(R.id.file_name)
        private var getFileButton : View = view.findViewById(R.id.get_file_icon)
        private var playButton : View = view.findViewById(R.id.play_file)
        private var fileDetailsView : TextView
            = view.findViewById(R.id.file_details)

        var fileEntry : BoltViewModel.BoltFileEntry? = null
            set(value) {
                field = value
                setGetFileVisibility()
                setPlayVisibility()
                setIcon()
                filenameView.text = value?.filename
                setDetails()
            }

        init {
            view.setOnClickListener({ _ ->
                adapter.toggleSelectedPosition(getBindingAdapterPosition())
                if (fileEntry?.isDirectory == true) {
                    fileEntry?.filename?.let { pushDirectory(it) }
                }
            })
            getFileButton.setOnClickListener({ _ ->
                fileEntry?.let({
                    pickFileToGet(it)
                })
            })
            playButton.setOnClickListener({ _ ->
                fileEntry?.let({ playFile(it) })
            })
            icon.setOnClickListener({ _ ->
                if (canDelete()) {
                    fileEntry?.let({ deleteFile(it) })
                }
            })
            viewModel.fileActionInfo.observe(
                viewLifecycleOwner,
                Observer<BoltViewModel.FileActionInfo> { _ ->
                    setGetFileVisibility()
                }
            )
        }

        private fun setGetFileVisibility() {
            if (!isViewModelConnected ||
                viewModel.isFileActionActive ||
                fileEntry?.isDirectory ?: true) {
                getFileButton.setVisibility(View.INVISIBLE)
            } else {
                getFileButton.setVisibility(View.VISIBLE)
            }
        }

        private fun setPlayVisibility() {
            if (!isViewModelConnected || !(fileEntry?.isPlayable ?: false))
                playButton.setVisibility(View.GONE)
            else
                playButton.setVisibility(View.VISIBLE)
        }

        private fun setIcon() {
            if (canDelete()) {
                icon.setImageResource(R.drawable.delete_icon)
                icon.contentDescription = getResources().getString(R.string.delete_description)
            } else if (fileEntry?.isDirectory == true) {
                icon.setImageResource(R.drawable.directory_icon)
                icon.contentDescription = getResources().getString(R.string.directory_description)
            } else {
                icon.setImageResource(R.drawable.file_icon)
                icon.contentDescription = getResources().getString(R.string.file_description)
            }
        }

        private fun canDelete() : Boolean {
            return (fileEntry?.isDirectory == false &&
                    getBindingAdapterPosition() == adapter.selectedPosition)
        }

        private fun setDetails() {
            fileEntry?.let {
                lateinit var size : String
                if (it.isDirectory) {
                    var entries = getResources().getString(R.string.entries)
                    size = "%d %s".format(it.size, entries)
                } else {
                    if (it.size < 1000)
                        size = "%dB".format(it.size)
                    else if (it.size < 1000000)
                        size = "%.1fKB".format(it.size / 1000.0)
                    else
                        size = "%.1fMB".format(it.size / 1000000.0)
                }

                val date =
                DateFormat.getDateInstance().format(
                    Date(it.timestamp * 1000L)
                )

                fileDetailsView.text = String.format(
                    getResources().getString(R.string.file_details),
                    size, date
                )
            }
        }
    }

    /**
     * Ask user if should covert FIT to GPX
     *
     * Returns yes or no
     */
    private suspend fun askFITToGPX(inFilename : String) : Boolean {
        if (!viewModel.isFITFile(inFilename))
            return false

        return suspendCoroutine { cont ->
            val msg =
                getResources().getString(R.string.ask_fit_to_gpx)

            AlertDialog.Builder(requireActivity())
                       .setMessage(msg)
                       .setIcon(android.R.drawable.ic_dialog_info)
                       .setPositiveButton(R.string.yes,
                                          { _, _ -> cont.resume(true) })
                       .setNegativeButton(R.string.no,
                                          { _, _ -> cont.resume(false)
                        })
                       .show()
        }
    }
}
