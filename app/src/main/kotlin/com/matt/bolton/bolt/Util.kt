
package com.matt.bolton.bolt

import java.util.zip.GZIPInputStream
import java.util.zip.GZIPOutputStream
import java.io.ByteArrayOutputStream
import java.io.ByteArrayInputStream
import java.nio.BufferUnderflowException
import java.nio.ByteBuffer
import java.util.UUID

fun Byte.toPositiveInt() : Int {
    val i = toInt()
    return if (i < 0) i + 256 else i
}

// from https://stackoverflow.com/a/51404278
fun byteArrayOfInts(vararg ints : Int) : ByteArray {
    return ByteArray(ints.size) { pos -> ints[pos].toByte() }
}

fun getBoltByteBuffer(capacity : Int) : ByteBuffer {
    val bytes = ByteArray(capacity)
    return getBoltByteBuffer(bytes)
}

fun getBoltByteBuffer(bytes : ByteArray) : ByteBuffer {
    return ByteBuffer.wrap(bytes).order(BOLT_ENDIAN)
}

fun getBoltByteBuffer(bytes : ByteArray,
                      offset : Int,
                      capacity : Int) : ByteBuffer {
    return ByteBuffer.wrap(bytes, offset, capacity).order(BOLT_ENDIAN)
}

fun ByteBuffer.putIntBytes(vararg ints : Int) {
    for (i in ints)
        put(i.toByte())
}

fun ByteBuffer.putBoltString(string : String) {
    put(string.toByteArray(BOLT_ENCODING))
}

/**
 * Reads a null-terminated string from the buffer (including the null)
 *
 * Throws BufferUnderflowException if falls off end
 */
fun ByteBuffer.getBoltNullString() : String {
    val bytes : MutableList<Byte> = mutableListOf()
    var b = get()
    while (b != 0.toByte()) {
        bytes.add(b)
        b = get()
    }
    return ByteArray(bytes.size,
                     { i -> bytes[i] }).toString(BOLT_ENCODING)
}

/**
 * Returns true if it managed to read the sequence of ints (interpreted
 * as bytes)
 */
fun ByteBuffer.getSequence(vararg ints : Int) : Boolean {
    for (i in ints) {
        val b = get()
        if (b != i.toByte())
            return false
    }
    return true
}

fun intToByteArray(value : Int, width : Int) : ByteArray {
    val bytes = ByteArray(width)
    return ByteBuffer.wrap(bytes).order(BOLT_ENDIAN).putInt(value).array()
}

/**
 * Send a message in chunks
 *
 * A message must be send in 20 byte chunks. This method takes care
 * of splitting the message into packages starting bodyCmd or
 * endCmd as needed, with the given message id.
 */
suspend fun sendLongMessage(bolt : BoltBT,
                            uuid : UUID,
                            bodyCmd : Int,
                            endCmd : Int,
                            msgID : Int,
                            msg : ByteArray) {
    val stream = ByteArrayInputStream(msg)
    var numBytesRead = 0
    var seq = 0

    while (numBytesRead < msg.size) {
        // three extra for cmd, id, and seq
        val packedMsg = ByteArray(BOLT_ALLOWED_MSG_BYTES + 3)
        val count = stream.read(packedMsg, 3, BOLT_ALLOWED_MSG_BYTES)
        if (count > -1) {
            numBytesRead += count
            val cmd = if (numBytesRead >= msg.size) endCmd else bodyCmd
            packedMsg.set(0, cmd.toByte())
            packedMsg.set(1, msgID.toByte())
            packedMsg.set(2, seq.toByte())
            seq += 1

            if (count == BOLT_ALLOWED_MSG_BYTES) {
                bolt.sendMessage(uuid, packedMsg)
            } else {
                // 2 is 3 bytes - 1 because inclusive
                val shortMsg = packedMsg.sliceArray(0..(2+count))
                bolt.sendMessage(uuid, shortMsg)
            }
        }
    }
}

/**
 * Debug function for displaying byte arrays
 */
fun readableBytes(bytes : ByteArray) : String {
    return bytes.joinToString(" ") { "%02x".format(it) }
}

/**
 * Returns a gzipped version of the bytes
 */
fun gzipBytes(bytes : ByteArray, offset : Int, limit : Int) : ByteArray {
    ByteArrayOutputStream(bytes.size).use { byteStream ->
        GZIPOutputStream(byteStream).use { zipStream ->
            zipStream.write(bytes, offset, limit)
        }
        return byteStream.toByteArray()
    }
}

fun gunzipBytes(bytes : ByteArray, offset : Int, limit : Int) : ByteArray {
    var result = ByteArray(0)
    ByteArrayInputStream(bytes, offset, limit).use { byteStream ->
        GZIPInputStream(byteStream).use { zipStream ->
            val chunk = ByteArray(FILE_CHUNK_SIZE)
            var count : Int
            do {
                count = zipStream.read(chunk, 0, FILE_CHUNK_SIZE)
                if (count >= 0) {
                    if (count < FILE_CHUNK_SIZE)
                        result += chunk.slice(0..(count - 1))
                    else
                        result += chunk
                }
            } while (count >= 0)
        }
        return result
    }
}
