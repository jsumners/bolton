
package com.matt.bolton.bolt

import kotlinx.coroutines.channels.ClosedReceiveChannelException

/**
 * Load a plan on the Bolt
 *
 * filename is the name of the file, the directory is determined by *
 * the directoryCode from PLAYABLE_PLANS_DIR_CODE constant. Note, the plan
 * file should have the .plan file extension.
 */
public class BCmdLoadPlan(val directoryCode : Byte,
                          val filename : String) : BoltCommand() {

    public class BCmdResLoadPlan(success : Boolean,
                                 val directoryCode : Byte,
                                 val filename : String)
            : BoltCommandResult(success)

    override suspend fun send(bolt : BoltBT) : BCmdResLoadPlan {
        val success = doSend(bolt)
        return BCmdResLoadPlan(success, directoryCode, filename)
    }

    private suspend fun doSend(bolt : BoltBT) : Boolean {
        var success = false

        val msgID = bolt.getNextMessageID()
        val responsePrefix = byteArrayOfInts(CMD_LOAD_PLAN_RES, msgID)
        val listener = BoltBytesListener(bolt,
                                         CONFIG_UUID,
                                         listOf(responsePrefix))
        try {
            listener.register()

            val sendName = filename.removeSuffix(PLAN_FILE_EXT)

            val msgBuffer = getBoltByteBuffer(3 + sendName.length)
            msgBuffer.putIntBytes(0)
            msgBuffer.put(directoryCode)
            msgBuffer.putBoltString(sendName)
            msgBuffer.putIntBytes(0)

            sendLongMessage(bolt,
                            CONFIG_UUID,
                            CMD_LOAD_PLAN_BODY,
                            CMD_LOAD_PLAN_END,
                            msgID,
                            msgBuffer.array())

            val res = listener.receive()

            success = res.last() == 0.toByte()

        } catch (e : ClosedReceiveChannelException) {
            // do nothing but return false
        } finally {
            listener.close()
        }

        return success
    }
}
