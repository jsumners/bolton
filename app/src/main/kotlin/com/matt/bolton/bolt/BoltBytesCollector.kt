
package com.matt.bolton.bolt

/**
 * Collects together split messages
 *
 * For example, a message split into repeated 15 01 <seq> ... and a
 * final 16 01 <seq> ... will * be passed on as simply 16 01 00 ... with a
 * long body. In this * example, 15 is the bodyCmd and 16 is the endCmd.
 */
public class BoltBytesCollector(val bodyCmd : Int, val endCmd : Int) {

    // map from msg id to seq number to msg
    private val bodyMsgs = HashMap<Byte, HashMap<Byte, ByteArray>>()
    // map from msg id to msg
    private val endMsgs = HashMap<Byte, ByteArray>()

    suspend fun
    getFullMessage(listener : BoltBytesListener) : ByteArray {
        while (true) {
            val msg = listener.receive()
            val fullMsg = collect(msg)
            if (fullMsg != null)
                return fullMsg
        }
    }

    /**
     * Add msg to collected bytes.
     *
     * A sanitised message is returned if completed, else null.
     */
    fun collect(msg : ByteArray) : ByteArray? {
        val msgID = msg.get(1)

        addPacket(msg)

        val fullMsg = getCompleteMsg(msgID)
        if (fullMsg != null) {
            deleteMsg(msgID)
            return fullMsg
        } else {
            return null
        }
    }

    private fun addPacket(msg : ByteArray) {
        val msgID = msg[1]
        val msgSeq = msg[2]

        // potential for lost messages to cause confusion when bolt reuses ids
        if (msg.get(0) == bodyCmd.toByte()) {
            if (!bodyMsgs.containsKey(msgID))
                bodyMsgs.put(msgID, HashMap<Byte, ByteArray>())
            val bodies : HashMap<Byte, ByteArray>? = bodyMsgs.get(msgID)
            bodies!!.put(msgSeq, msg)
        } else {
            endMsgs.put(msgID, msg)
        }
    }

    /**
     * Returns complete message for msg_id if received, else null
     */
    @Throws(BoltBT.BoltBTException::class)
    private fun getCompleteMsg(msgID : Byte) : ByteArray? {
        if (!endMsgs.containsKey(msgID))
            return null

        val msgEnd = endMsgs.get(msgID)
        var lastSeqNo = msgEnd?.get(2)?.toPositiveInt()

        if (lastSeqNo == null || msgEnd == null) {
            throw BoltBT.BoltBTException("Got incoherent sequence of messages.")
        } else if (lastSeqNo == 0) {
            return msgEnd
        } else {
            val bodies = bodyMsgs.get(msgID)

            if (bodies == null || bodies.size < lastSeqNo) {
                return null
            }

            val msgLen = 3 + BOLT_ALLOWED_MSG_BYTES * (lastSeqNo + 1)
            var buffer = getBoltByteBuffer(msgLen)

            buffer.put(msgEnd, 0, 2)
            buffer.putIntBytes(0)

            for (idx in 0..(lastSeqNo-1)) {
                val body = bodies.get(idx.toByte())
                if (body == null)
                    throw BoltBT.BoltBTException("Could not decode sequence of messages")
                buffer.put(body, 3, body.size - 3)
            }

            buffer.put(msgEnd, 3, msgEnd.size - 3)

            return buffer.array()
        }
    }

    /**
     * Deletes msg_id from history
     */
    private fun deleteMsg(msgID : Byte) {
        if (bodyMsgs.containsKey(msgID))
            bodyMsgs.remove(msgID)
        if (endMsgs.containsKey(msgID))
            endMsgs.remove(msgID)
    }
}
